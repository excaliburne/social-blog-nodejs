import Search from './modules/Search';
import Chat from './modules/Chat';
import RegistrationForm from './modules/registrationFrom';

if (document.querySelector("#registration-form")) {
    new RegistrationForm()
}

// if user is logged in create Search object
if (document.querySelector(".header-search-icon")) {
    new Search()
}

// if user click chat icon, create Chat object
if (document.querySelector('#chat-wrapper')) {
    new Chat()
}