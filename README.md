# Social Blog in NodeJS
---
Fairly simple social network/blogging app built with NodeJS/Express/Webpack and MongoDB. Doesn't look fancy and it's not meant to be.

## Features
---
- Login/Register
- Link profile picture with 'gravatar'
- Create a post (simple title & body content)
- Delete/update posts that you own
- Follow any user and see their latest post in your feed
- Search posts
- Messenger-like chat with socket.io where all users can interact (no private rooms)
- Small API to create/delete and retrieve posts.

## Tech used
---
[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png" height="70">](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
[<img src="https://thekenyandev.com/static/nodejs-ea6d8fe57ed02c773ad10ca3003b2451.png
" height="70">](https://nodejs.org/en/)
[<img src="https://buttercms.com/static/images/tech_banners/ExpressJS.png" height="70">](https://expressjs.com/)
[<img src="https://raw.githubusercontent.com/webpack/media/master/logo/icon-square-big.png" height="70">](https://webpack.js.org/)
[<img src="https://brand.heroku.com/static/media/heroku-logotype-vertical.f7e1193f.svg" height="70">](https://dashboard.heroku.com/)
[<img src="https://www.sodifrance.fr/blog/wp-content/uploads/2014/11/mongodb-nosql-logo.png" height="70">](https://www.mongodb.com/)


## Screenshots
---
### > feed
![alt text](https://i.imgur.com/5QXg07Q.png "Feed")

### > profile
![alt text](https://i.imgur.com/tvVHDiy.png "Profile")

### > create post
![alt text](https://i.imgur.com/IDOLt5c.png "Profile")

### > chat
![alt text](https://i.imgur.com/pr6dPQ0.png "Profile")

### > search
![alt text](https://i.imgur.com/GADhdWP.png "Profile")

## API documentation
---
`/api/login` (via post request)
- Log you in to application by giving you an access token.

`/api/create-post` (via post request)
- Create a post. "title", "body" and "token" necessary in header.

`/api/:postid` (via delete request)
- Replace ':postid' with id of post you want to delete
- Send a http delete request with an access token in header.

`/api/getPostsByAuthor/:username` (via get request)
- Get list of posts given username
