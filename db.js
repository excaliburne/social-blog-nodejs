const mongodb = require('mongodb')

const connectionString = process.env.SOCIAL_DB_STRING

mongodb.connect(connectionString, { useUnifiedTopology: true }, function(err, client) {
    module.exports = client
    const app = require('./app')
    app.listen(process.env.PORT)
})

console.log(`listening on port: ${process.env.PORT}`)